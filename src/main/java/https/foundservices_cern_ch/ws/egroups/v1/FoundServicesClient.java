package https.foundservices_cern_ch.ws.egroups.v1;

import https.foundservices_cern_ch.ws.egroups.v1.egroupswebservice.EgroupsService;
import https.foundservices_cern_ch.ws.egroups.v1.egroupswebservice.EgroupsWebService;
import https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema.*;

import javax.xml.ws.BindingProvider;
import java.util.Map;
import java.util.Random;

/**
 * Demo how to use the e-groups foundservices from java
 */
public class FoundServicesClient {

    public static void main(String[] args) {

        // This will print the FULL request in your console
        // WARNING: your credentials encoded in base64 TOO!!!
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

        EgroupsWebService egroupsWebService = new EgroupsWebService();
        EgroupsService client = egroupsWebService.getEgroupsServiceSoap11();
        Map<String, Object> requestContext = ((BindingProvider)client).getRequestContext();
        requestContext.put(BindingProvider.USERNAME_PROPERTY, System.getenv("USERNAME"));
        requestContext.put(BindingProvider.PASSWORD_PROPERTY, System.getenv("PASSWORD"));

        // Get a test e-group and show its description in the console
        FindEgroupByNameRequest findEgroupByNameRequest = new FindEgroupByNameRequest();
        findEgroupByNameRequest.setName("test-INC2959270");
        FindEgroupByNameResponse findEgroupByNameResponse = client.findEgroupByName(findEgroupByNameRequest);
        EgroupType egroupType = findEgroupByNameResponse.getResult();
        System.out.println(egroupType.getDescription());

        // Now do some manipulation with it, like updating a comment, adding a member and admin egroup
        SynchronizeEgroupRequest synchronizeEgroupRequest = new SynchronizeEgroupRequest();
        MemberType memberType = createMemberType("10395640", MemberTypeCode.DYNAMIC_EGROUP, "fap-dep-bc-pl");
        egroupType.getMembers().add(memberType);
        String comments = egroupType.getComments();
        egroupType.setComments(comments + " " + generateRandom());
        egroupType.setAdministratorEgroup("cms-web-access-admins");
        synchronizeEgroupRequest.setEgroup(egroupType);
        SynchronizeEgroupResponse synchronizeEgroupResponse = client.synchronizeEgroup(synchronizeEgroupRequest);
        checkResponse(synchronizeEgroupResponse);

        // Remove the new added member
        egroupType.getMembers().remove(memberType);
        synchronizeEgroupRequest.setEgroup(egroupType);
        synchronizeEgroupResponse = client.synchronizeEgroup(synchronizeEgroupRequest);
        checkResponse(synchronizeEgroupResponse);
        
    }

    private static MemberType createMemberType(String id, MemberTypeCode memberTypeCode, String name) {
        MemberType memberType = new MemberType();
        memberType.setID(Long.valueOf(id));
        memberType.setType(MemberTypeCode.DYNAMIC_EGROUP);
        memberType.setName(name);
        return memberType;
    }

    private static void checkResponse(SynchronizeEgroupResponse synchronizeEgroupResponse) {
        if(synchronizeEgroupResponse.getError()!=null){
            System.out.println(synchronizeEgroupResponse.getError().getCode()
                    + " " + synchronizeEgroupResponse.getError().getMessage());
        } else {
            System.out.println(synchronizeEgroupResponse.getWarnings());
        }
    }

    /**
     * From https://www.baeldung.com/java-random-string
     * @return
     */
    private static String generateRandom() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }
}
