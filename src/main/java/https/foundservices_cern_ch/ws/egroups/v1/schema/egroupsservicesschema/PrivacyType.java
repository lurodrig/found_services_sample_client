
package https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PrivacyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="PrivacyType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Members"/&gt;
 *     &lt;enumeration value="Open"/&gt;
 *     &lt;enumeration value="Administrators"/&gt;
 *     &lt;enumeration value="Users"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PrivacyType")
@XmlEnum
public enum PrivacyType {

    @XmlEnumValue("Members")
    MEMBERS("Members"),
    @XmlEnumValue("Open")
    OPEN("Open"),
    @XmlEnumValue("Administrators")
    ADMINISTRATORS("Administrators"),
    @XmlEnumValue("Users")
    USERS("Users");
    private final String value;

    PrivacyType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PrivacyType fromValue(String v) {
        for (PrivacyType c: PrivacyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
