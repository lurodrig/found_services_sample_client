
package https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PrivilegeTypeCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="PrivilegeTypeCode"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SeeMembers"/&gt;
 *     &lt;enumeration value="Admin"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PrivilegeTypeCode")
@XmlEnum
public enum PrivilegeTypeCode {

    @XmlEnumValue("SeeMembers")
    SEE_MEMBERS("SeeMembers"),
    @XmlEnumValue("Admin")
    ADMIN("Admin");
    private final String value;

    PrivilegeTypeCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PrivilegeTypeCode fromValue(String v) {
        for (PrivilegeTypeCode c: PrivilegeTypeCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
