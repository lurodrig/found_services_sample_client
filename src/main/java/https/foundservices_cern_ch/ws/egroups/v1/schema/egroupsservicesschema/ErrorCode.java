
package https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ErrorCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="ErrorCode"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NOT_VALID_USER"/&gt;
 *     &lt;enumeration value="NOT_FOUND"/&gt;
 *     &lt;enumeration value="EGROUP_NAME_BAD_FORMATED"/&gt;
 *     &lt;enumeration value="INSUFFICIENT_PRIVILEGES"/&gt;
 *     &lt;enumeration value="PERSON_ID_NOT_FOUND"/&gt;
 *     &lt;enumeration value="EXPIRE_DATE_BAD_FORMATTED"/&gt;
 *     &lt;enumeration value="NAME_AND_ID_NOT_CORRESPONDS"/&gt;
 *     &lt;enumeration value="EGROUP_RECENTLY_DELETED"/&gt;
 *     &lt;enumeration value="EMAIL_PROP_ARCHIVE_NOT_VALID"/&gt;
 *     &lt;enumeration value="EMAIL_PROP_MAIL_SIZE_NOT_VALID"/&gt;
 *     &lt;enumeration value="EXPIRATION_DATE_MANDATORY"/&gt;
 *     &lt;enumeration value="EXPIRATION_DATE_NOT_VALID"/&gt;
 *     &lt;enumeration value="INTERNAL_DB_ERROR"/&gt;
 *     &lt;enumeration value="DATABASE_CONFIGURATION_NOT_FOUND"/&gt;
 *     &lt;enumeration value="EGROUP_MEMBER_OF_ANOTHER"/&gt;
 *     &lt;enumeration value="NOT_MAILING_SEGURITY_USAGE"/&gt;
 *     &lt;enumeration value="IS_ALREADY_MEMBER"/&gt;
 *     &lt;enumeration value="TOPIC_ALREADY_EXISTS"/&gt;
 *     &lt;enumeration value="ALIAS_MUST_HAVE_HYPHEN"/&gt;
 *     &lt;enumeration value="ALIAS_ALREADY_EXISTS"/&gt;
 *     &lt;enumeration value="NAME_ALREADY_RESERVED"/&gt;
 *     &lt;enumeration value="NAME_TOO_LONG"/&gt;
 *     &lt;enumeration value="EGROUP_ALREADY_EXISTS"/&gt;
 *     &lt;enumeration value="MEMBER_NOT_FOUND"/&gt;
 *     &lt;enumeration value="ALIAS_NOT_FOUND"/&gt;
 *     &lt;enumeration value="SELF_EGROUP_ALREADY_EXISTS"/&gt;
 *     &lt;enumeration value="ALREADY_ACTIVE"/&gt;
 *     &lt;enumeration value="IS_BLOCKED"/&gt;
 *     &lt;enumeration value="MUST_BE_MODERATOR"/&gt;
 *     &lt;enumeration value="STATUS_CHANGE_NOT_ALLOWED"/&gt;
 *     &lt;enumeration value="EXPIRATION_DATE_CANT_BE_PROLONGUED"/&gt;
 *     &lt;enumeration value="BLOCKING_REASON_UNDEFINED"/&gt;
 *     &lt;enumeration value="USAGE_TYPE_NOT_VALID"/&gt;
 *     &lt;enumeration value="NOT_LOGGED"/&gt;
 *     &lt;enumeration value="ALREADY_DELETED"/&gt;
 *     &lt;enumeration value="NAME_MUST_HAVE_HYPHEN"/&gt;
 *     &lt;enumeration value="IS_ALREADY_ALLOWED_TO_POST"/&gt;
 *     &lt;enumeration value="HAS_ALREADY_PRIVILEGE"/&gt;
 *     &lt;enumeration value="OWNER_ID_NOT_FOUND"/&gt;
 *     &lt;enumeration value="WARNING"/&gt;
 *     &lt;enumeration value="UNEXPECTED_ERROR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ErrorCode")
@XmlEnum
public enum ErrorCode {

    NOT_VALID_USER,
    NOT_FOUND,
    EGROUP_NAME_BAD_FORMATED,
    INSUFFICIENT_PRIVILEGES,
    PERSON_ID_NOT_FOUND,
    EXPIRE_DATE_BAD_FORMATTED,
    NAME_AND_ID_NOT_CORRESPONDS,
    EGROUP_RECENTLY_DELETED,
    EMAIL_PROP_ARCHIVE_NOT_VALID,
    EMAIL_PROP_MAIL_SIZE_NOT_VALID,
    EXPIRATION_DATE_MANDATORY,
    EXPIRATION_DATE_NOT_VALID,
    INTERNAL_DB_ERROR,
    DATABASE_CONFIGURATION_NOT_FOUND,
    EGROUP_MEMBER_OF_ANOTHER,
    NOT_MAILING_SEGURITY_USAGE,
    IS_ALREADY_MEMBER,
    TOPIC_ALREADY_EXISTS,
    ALIAS_MUST_HAVE_HYPHEN,
    ALIAS_ALREADY_EXISTS,
    NAME_ALREADY_RESERVED,
    NAME_TOO_LONG,
    EGROUP_ALREADY_EXISTS,
    MEMBER_NOT_FOUND,
    ALIAS_NOT_FOUND,
    SELF_EGROUP_ALREADY_EXISTS,
    ALREADY_ACTIVE,
    IS_BLOCKED,
    MUST_BE_MODERATOR,
    STATUS_CHANGE_NOT_ALLOWED,
    EXPIRATION_DATE_CANT_BE_PROLONGUED,
    BLOCKING_REASON_UNDEFINED,
    USAGE_TYPE_NOT_VALID,
    NOT_LOGGED,
    ALREADY_DELETED,
    NAME_MUST_HAVE_HYPHEN,
    IS_ALREADY_ALLOWED_TO_POST,
    HAS_ALREADY_PRIVILEGE,
    OWNER_ID_NOT_FOUND,
    WARNING,
    UNEXPECTED_ERROR;

    public String value() {
        return name();
    }

    public static ErrorCode fromValue(String v) {
        return valueOf(v);
    }

}
