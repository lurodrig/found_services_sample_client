# Foundservices sample client

## What?

Demo of a Java client for https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/EgroupsWebService.wsdl

## How?

- Set your `JAVA_HOME` to java 8 e.g. ` JAVA_HOME=/your/local/path/jdk-1.8.0_281`
- `mvn compile` will execute the `cxf-codegen-plugin:3.4.4:wsdl2java` see more at [apache cfx](https://cxf.apache.org/)
- The generated code for the service stubs will be generated at 
`<sourceRoot>${project.build.sourceDirectory}</sourceRoot>` see your `pom.xml`
- You will need a [service account](https://account.cern.ch/account/) that belongs to the 
[foundservices-egroups-create](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10059073)

**Horrible hack**: I have copied the .wsdl to the project's `resources` folder. Also, in the `foundservices.wsdl` notice 
the `schemaLocation="src/main/resources/EgroupsServicesSchema.xsd"`. The problem is that foundservices.cern.ch is behind
basic authentication.

## What next?

Simply code your business logic in the `FoundServicesClient`, happy coding!!!

