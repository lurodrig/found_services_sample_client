@javax.xml.bind.annotation.XmlSchema(namespace = "https://foundservices.cern.ch/ws/egroups/v1/schema/EgroupsServicesSchema", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema;
