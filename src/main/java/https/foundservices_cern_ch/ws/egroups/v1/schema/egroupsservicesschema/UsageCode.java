
package https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UsageCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="UsageCode"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EgroupsOnly"/&gt;
 *     &lt;enumeration value="SecurityMailing"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "UsageCode")
@XmlEnum
public enum UsageCode {

    @XmlEnumValue("EgroupsOnly")
    EGROUPS_ONLY("EgroupsOnly"),
    @XmlEnumValue("SecurityMailing")
    SECURITY_MAILING("SecurityMailing");
    private final String value;

    UsageCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UsageCode fromValue(String v) {
        for (UsageCode c: UsageCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
