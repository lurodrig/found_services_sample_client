
package https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SelfsubscriptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="SelfsubscriptionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Closed"/&gt;
 *     &lt;enumeration value="Open"/&gt;
 *     &lt;enumeration value="Members"/&gt;
 *     &lt;enumeration value="Users"/&gt;
 *     &lt;enumeration value="OpenWithAdminApproval"/&gt;
 *     &lt;enumeration value="UsersWithAdminApproval"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SelfsubscriptionType")
@XmlEnum
public enum SelfsubscriptionType {

    @XmlEnumValue("Closed")
    CLOSED("Closed"),
    @XmlEnumValue("Open")
    OPEN("Open"),
    @XmlEnumValue("Members")
    MEMBERS("Members"),
    @XmlEnumValue("Users")
    USERS("Users"),
    @XmlEnumValue("OpenWithAdminApproval")
    OPEN_WITH_ADMIN_APPROVAL("OpenWithAdminApproval"),
    @XmlEnumValue("UsersWithAdminApproval")
    USERS_WITH_ADMIN_APPROVAL("UsersWithAdminApproval");
    private final String value;

    SelfsubscriptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SelfsubscriptionType fromValue(String v) {
        for (SelfsubscriptionType c: SelfsubscriptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
