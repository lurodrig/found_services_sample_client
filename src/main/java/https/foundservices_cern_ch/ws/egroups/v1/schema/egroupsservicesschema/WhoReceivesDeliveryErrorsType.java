
package https.foundservices_cern_ch.ws.egroups.v1.schema.egroupsservicesschema;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WhoReceivesDeliveryErrorsType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="WhoReceivesDeliveryErrorsType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="GroupOwner"/&gt;
 *     &lt;enumeration value="Sender"/&gt;
 *     &lt;enumeration value="None"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WhoReceivesDeliveryErrorsType")
@XmlEnum
public enum WhoReceivesDeliveryErrorsType {

    @XmlEnumValue("GroupOwner")
    GROUP_OWNER("GroupOwner"),
    @XmlEnumValue("Sender")
    SENDER("Sender"),
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    WhoReceivesDeliveryErrorsType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WhoReceivesDeliveryErrorsType fromValue(String v) {
        for (WhoReceivesDeliveryErrorsType c: WhoReceivesDeliveryErrorsType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
